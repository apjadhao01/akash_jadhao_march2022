import java.util.*;
public class reverseLinkedList{
	
	class node{
		int data;
		node next;
		int size;
		
		node(int data){
		this.data = data;
		this.next = null;
		this.size = size;
		}
	}
	
	node head = null;
	
	public void add(int data,int size){
		node newNode = new node(data);
		if(head == null){
			head = newNode;
			
			return;
		}
		node currNode = head;
		while(currNode.next != null){
	    currNode = currNode.next;
		}
		currNode.next = newNode;
		
	}
	
	public void display(){
		if(head == null){
			System.out.print("list is empty");
			return;
		}
		node currNode = head;
		while( currNode != null){
			System.out.print(currNode.data+" ");
			currNode = currNode.next;
		}
	}
	
	public void reverse(){
		if(head == null && head.next == null)
			return;
		node prev = head;
		node currNode = head.next;
		while(currNode != null){
			node next = currNode.next;
			
			currNode.next = prev;
			
			prev = currNode;
			currNode = next;
		}
		head.next = null;
		head = prev;
	}
	
	public static void main(String[] args){
		
		reverseLinkedList l = new reverseLinkedList();
		
		Scanner sc = new Scanner(System.in);
		//System.out.print(" enter size ");
		int size = sc.nextInt();
		
		//System.out.print(" enter data ");
		for(int g =0 ;g < size ; g++){
			//System.out.print(" enter data at "+g+" : ");
		int d = sc.nextInt();
		l.add(d,size);
		}
		//l.display();
		
		l.reverse();
		
		System.out.println("\noutput");
		
		l.display();
		
	}
}