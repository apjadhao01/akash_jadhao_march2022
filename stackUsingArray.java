public class stackUsingArray{
	
	static int array[] = new int[10];
	static int top1 = 5;
	static int top2 = array.length-1;
	
	public static void push1(int data){
		//int top1 = 5;
		array[top1] = data;
		top1--;
		
	}
	
	public static void push2(int data){
		//int top2 = array.length-1;
		array[top2] = data;
		top2--;
		
	}
	public static void main(String[] args){
		
		//int array[] = new int[10];
		
		//stackUsingArray s = new stackUsingArray(array);
		push1(5);
		push2(10);
		push2(15);
		push1(11);
		push2(7);
		push2(40);
		
		System.out.println("\npopped element from stack 1 is "+array[++top1]);
		System.out.println("popped element from stack 2 is "+array[++top2]);
	}
}